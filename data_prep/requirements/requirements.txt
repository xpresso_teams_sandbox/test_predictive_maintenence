autofeat==1.1.3
numpy==1.18.5
seaborn==0.9.0
pandas==0.25.3
Keras==2.4.3
xgboost==1.2.1
matplotlib==3.0.2
scikit_learn==0.23.2
